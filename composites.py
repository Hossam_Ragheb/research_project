# -*- coding: utf-8 -*-
"""
Created on Fri Apr 29 17:15:53 2016

@author: hossamragheb
"""
import numpy as np
import scipy
from scipy.sparse.linalg.interface import aslinearoperator
from sympy import Integral, Matrix, pi, pprint
import matplotlib.pyplot as plt
import pylab
from pandas import *

#init_printing(wrap_line=False)

def TestReddy():
    Modulus = np.array([175*10**9, 7*10**9, 0.25, 0.02, 3.5*10**9, 1.4*10**9, 3.5*10**9])
    layers = LayersT(np.array([0,90,0]),1,np.array([45,-45]),0,0.05)
    return Modulus, layers

def TestCLPT():
    Modulus = np.array([20010000, 1301000, 0.3, 0.02, 1001000, 286000, 1001000])
    layers = LayersT(np.array([0,45]),1,np.array([45,-45]),0,0.005)
    return Modulus, layers

########################
def Paperdata():
    Modulus = np.array([125000000000, 9588000000, 0.35, 0.02, 5414000000, 1454000000, 5535000000])
    layers = LayersT(np.array([90, 15, -15, 90, 45, -45, 45, -45, 45, -45]),5,np.array([45,-45]),3,0.000075)
    return Modulus, layers
######################Local Strain Distribution#############################
def local_strain_distribution2(Modulus,layers,theory,global_loads,axis=0): 
    """
    Modulus: (float,array),  [E1,E2,v12,v21,G12,G23,G13]
    layers : (float, array), layers data in the form of [angle,thickness,Zk]
    theory : string, 'CLPT', 'FSDT', 'TSDT'
    global_loads: (float array), [Nxx,Nyy,Nxy,Mxx,Myy,Mxy,Pxx,Pyy,Pxy,Qy,Qx,Ry,Rx]
    axis : 0 -> exx, 1-->eyy,2-->ezz=0, 3-->gamma_yz, 4-->gamma_xz, 5--> gamma_xy
    OUTPUTS:
    
    Distribution : (float) array with load vs thickness
    """
    N=10
    Distribution_s=[]
    for i, layer in enumerate(layers):
        if(theory == 'CLPT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Ply_Strains_S = np.array([Ply_Strains[0],Ply_Strains[1],0,
                                    0,0,Ply_Strains[2]]) 
                Ply_LStrains = localstrains(Ply_Strains_S,layer[0])
                Strain = Ply_LStrains[axis]
                Distribution_s.append([z,Strain])
        if(theory == 'FSDT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Ply_Strains_S = np.array([Ply_Strains[0],Ply_Strains[1],0,
                                    Ply_Strains[3],Ply_Strains[4],Ply_Strains[2]]) 
                Ply_LStrains = localstrains(Ply_Strains_S,layer[0])
                Strain = Ply_LStrains[axis]
                Distribution_s.append([z,Strain])
        if(theory == 'TSDT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Ply_Strains_S = np.array([Ply_Strains[0],Ply_Strains[1],0,
                                    Ply_Strains[3],Ply_Strains[4],Ply_Strains[2]]) 
                Ply_LStrains = localstrains(Ply_Strains_S,layer[0])
                Strain = Ply_LStrains[axis]
                Distribution_s.append([z,Strain])
    print(Distribution_s)
    x = [row[1] for row in Distribution_s]
    y = [row[0] for row in Distribution_s]
    plt.plot(x,y,label='Strain')  
    plt.ylim(layers[len(layers)-1][3], layers[0][2])
    plt.xlim(np.min(x),np.max(x))
    plt.axhline(0,linestyle='--', color='black')
    plt.axvline(0, color='black')
    plt.fill_betweenx(y,x,0.0)
    plt.grid(True)
    pylab.show()
    return Distribution_s
######################Local Strain Distribution#############################
                        #VOID#
###########################################################################
def local_strain_distribution(Modulus,layers,theory,global_loads,axis=0): 
    """
    Modulus: (float,array),  [E1,E2,v12,v21,G12,G23,G13]
    layers : (float, array), layers data in the form of [angle,thickness,Zk]
    theory : string, 'CLPT', 'FSDT', 'TSDT'
    global_loads: (float array), [Nxx,Nyy,Nxy,Mxx,Myy,Mxy,Pxx,Pyy,Pxy,Qy,Qx,Ry,Rx]
    axis : 0 -> exx, 1-->eyy,2-->ezz=0, 3-->gamma_yz, 4-->gamma_xz, 5--> gamma_xy
    OUTPUTS:
    
    Distribution : (float) array with load vs thickness
    """
    Distribution_s=[]
    for i, layer in enumerate(layers):
        if(theory == 'CLPT'):
            Res_Strains = solver(Modulus,layers,theory,global_loads)
            Ply_Strains_top = laminastrains(Res_Strains,layer[2],theory)
            Ply_Strains_bottom = laminastrains(Res_Strains,layer[3],theory)
            Ply_Strains_topS = np.array([Ply_Strains_top[0],Ply_Strains_top[1],0,
                                0,0,Ply_Strains_top[2]]) 
            Ply_Strains_bottomS = np.array([Ply_Strains_bottom[0],Ply_Strains_bottom[1],0,
                                0,0,Ply_Strains_bottom[2]]) 
            Ply_LStrains_top = localstrains(Ply_Strains_topS,layer[0])
            Ply_LStrains_bottom = localstrains(Ply_Strains_bottomS,layer[0])           
            Strain_top = Ply_LStrains_top[axis]
            Strain_bottom = Ply_LStrains_bottom[axis]
            Distribution_s.append([layer[2],Strain_top])
            Distribution_s.append([layer[3],Strain_bottom])

        if(theory == 'FSDT'):
            Res_Strains = solver(Modulus,layers,theory,global_loads)
            Ply_Strains_top = laminastrains(Res_Strains,layer[2],theory)
            Ply_Strains_bottom = laminastrains(Res_Strains,layer[3],theory)
            Ply_Strains_topS = np.array([Ply_Strains_top[0],Ply_Strains_top[1],0,
                                Ply_Strains_top[3],Ply_Strains_top[4],Ply_Strains_top[2]]) 
            Ply_Strains_bottomS = np.array([Ply_Strains_bottom[0],Ply_Strains_bottom[1],0,
                                Ply_Strains_bottom[3],Ply_Strains_bottom[4],Ply_Strains_bottom[2]]) 
            Ply_LStrains_top = localstrains(Ply_Strains_topS,layer[0])
            Ply_LStrains_bottom = localstrains(Ply_Strains_bottomS,layer[0])
            Strain_top = Ply_LStrains_top[axis]
            Strain_bottom = Ply_LStrains_bottom[axis]
            Distribution_s.append([layer[2],Strain_top])
            Distribution_s.append([layer[3],Strain_bottom])

        if(theory == 'TSDT'):
            Res_Strains = solver(Modulus,layers,theory,global_loads)
            Ply_Strains_top = laminastrains(Res_Strains,layer[2],theory)
            Ply_Strains_bottom = laminastrains(Res_Strains,layer[3],theory)
            Ply_Strains_topS = np.array([Ply_Strains_top[0],Ply_Strains_top[1],0,
                                Ply_Strains_top[3],Ply_Strains_top[4],Ply_Strains_top[2]]) 
            Ply_Strains_bottomS = np.array([Ply_Strains_bottom[0],Ply_Strains_bottom[1],0,
                                Ply_Strains_bottom[3],Ply_Strains_bottom[4],Ply_Strains_bottom[2]]) 
            Ply_LStrains_top = localstrains(Ply_Strains_topS,layer[0])
            Ply_LStrains_bottom = localstrains(Ply_Strains_bottomS,layer[0])
            Strain_top = Ply_LStrains_top[axis]
            Strain_bottom = Ply_LStrains_bottom[axis]
            Distribution_s.append([layer[2],Strain_top])
            Distribution_s.append([layer[3],Strain_bottom])
    print(Distribution_s)
    x = [row[1] for row in Distribution_s]
    y = [row[0] for row in Distribution_s]
    plt.plot(x,y,label='Strain')  
    plt.ylim(layers[len(layers)-1][3], layers[0][2])
    plt.xlim(np.min(x),np.max(x))
    plt.axhline(0,linestyle='--', color='black')
    plt.axvline(0, color='black')
    plt.fill_betweenx(y,x,0.0)
    plt.grid(True)
    pylab.show()
    return Distribution_s
############################################################################
def local_stress_distribution2(Modulus,layers,theory,global_loads,axis=0):
    """
        Modulus: (float,array),  [E1,E2,v12,v21,G12,G23,G13]
    layers : (float, array), layers data in the form of [angle,thickness,Zk]
    theory : string, 'CLPT', 'FSDT', 'TSDT'
    global_loads: (float array), [Nxx,Nyy,Nxy,Mxx,Myy,Mxy,Pxx,Pyy,Pxy,Qy,Qx,Ry,Rx]
    axis : 0 -> Sxx, 1-->Syy,2-->Szz=0, 3-->S_yz, 4-->S_xz, 5--> S_xy
    OUTPUTS:
    
    Distribution : (float) array with load vs thickness
    """
    N=10
    Distribution=[]
    for i, layer in enumerate(layers):
        if(theory == 'CLPT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Q_bar = Constitutive(Modulus[0],Modulus[1],
                Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
                Ply_stresses = np.dot(Q_bar,Ply_Strains)            
                Ply_stresses_S = np.array([Ply_stresses[0],Ply_stresses[1],0.0,
                                    0.0,0.0,Ply_stresses[2]])                                    
                Ply_Lstresses = localstress(Ply_stresses_S,layer[0])            
                Stress = Ply_Lstresses[axis]
                Distribution.append([z,Stress])
        if(theory == 'FSDT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Q_bar,Q_shear = Constitutive(Modulus[0],Modulus[1],
                Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
                Ply_stresses = np.dot(Q_bar,Ply_Strains[:3])
                Ply_stresses_shear = np.dot(Q_shear,Ply_Strains[3:])     
                Stress_full = np.array([Ply_stresses[0],Ply_stresses[1],0,
                                            Ply_stresses_shear[0],Ply_stresses_shear[1],Ply_stresses[2]])
                Ply_Lstresses = localstress(Stress_full,layer[0])     
                Stress = Ply_Lstresses[axis]
                Distribution.append([z,Stress])

        if(theory == 'TSDT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)            
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Q_bar,Q_shear = Constitutive(Modulus[0],Modulus[1],
                Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
                Ply_stresses = np.dot(Q_bar,Ply_Strains[:3])
                Ply_stresses_shear = np.dot(Q_shear,Ply_Strains[3:])
                Stress_full = np.array([Ply_stresses[0],Ply_stresses[1],0,
                                            Ply_stresses_shear[0],Ply_stresses_shear[1],Ply_stresses[2]])
                Ply_Lstresses = localstress(Stress_full,layer[0])     
                Stress = Ply_Lstresses[axis]
                Distribution.append([z,Stress])
    print(Distribution)
    x = [row[1] for row in Distribution]
    y = [row[0] for row in Distribution]
    plt.plot(x,y,label='Strain')  
    plt.ylim(layers[len(layers)-1][3], layers[0][2])
    plt.xlim(np.min(x),np.max(x))
    plt.axhline(0,linestyle='--', color='black')
    plt.axvline(0, color='black')
    plt.fill_betweenx(y,x,0.0)
    plt.grid(True)
    pylab.show()
    return Distribution
######################Local Stress Distribution#############################
                        #VOID#
###########################################################################
def local_stress_distribution(Modulus,layers,theory,global_loads,axis=0):
    """
    Modulus: (float,array),  [E1,E2,v12,v21,G12,G23,G13]
    layers : (float, array), layers data in the form of [angle,thickness,Zk]
    theory : string, 'CLPT', 'FSDT', 'TSDT'
    global_loads: (float array), [Nxx,Nyy,Nxy,Mxx,Myy,Mxy,Pxx,Pyy,Pxy,Qy,Qx,Ry,Rx]
    axis : 0 -> Sxx, 1-->Syy,2-->Szz=0, 3-->S_yz, 4-->S_xz, 5--> S_xy
    OUTPUTS:
    
    Distribution : (float) array with load vs thickness
    """
    Distribution=[]
    for i, layer in enumerate(layers):
        if(theory == 'CLPT'):
            Res_Strains = solver(Modulus,layers,theory,global_loads)
            Ply_Strains_top = laminastrains(Res_Strains,layer[2],theory)
            Ply_Strains_bottom = laminastrains(Res_Strains,layer[3],theory)
            Q_bar = Constitutive(Modulus[0],Modulus[1],
            Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
            
            Ply_stresses_top = np.dot(Q_bar,Ply_Strains_top)
            Ply_stresses_bottom = np.dot(Q_bar,Ply_Strains_bottom)
            
            Ply_stresses_topS = np.array([Ply_stresses_top[0],Ply_stresses_top[1],0,
                                0,0,Ply_stresses_top[2]])            
            Ply_stresses_bottomS = np.array([Ply_stresses_bottom[0],Ply_stresses_bottom[1],0,
                                0,0,Ply_stresses_bottom[2]])                        
            Ply_Lstresses_top = localstress(Ply_stresses_topS,layer[0])
            Ply_Lstresses_bottom = localstress(Ply_stresses_bottomS,layer[0])
            
            Stress_top = Ply_Lstresses_top[axis]
            Stress_bottom = Ply_Lstresses_bottom[axis]
            Distribution.append([layer[2],Stress_top])
            Distribution.append([layer[3],Stress_bottom])

        if(theory == 'FSDT'):
            Res_Strains = solver(Modulus,layers,theory,global_loads)
            Ply_Strains_top = laminastrains(Res_Strains,layer[2],theory)
            Ply_Strains_bottom = laminastrains(Res_Strains,layer[3],theory)
            Q_bar,Q_shear = Constitutive(Modulus[0],Modulus[1],
            Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
            Ply_stresses_top = np.dot(Q_bar,Ply_Strains_top[:3])
            Ply_stresses_top_shear = np.dot(Q_shear,Ply_Strains_top[3:])
            Ply_stresses_bottom = np.dot(Q_bar,Ply_Strains_bottom[:3])
            Ply_stresses_bottom_shear = np.dot(Q_shear,Ply_Strains_bottom[3:])     
            Stress_top_full = np.array([Ply_stresses_top[0],Ply_stresses_top[1],0,
                                        Ply_stresses_top_shear[0],Ply_stresses_top_shear[1],Ply_stresses_top[2]])
            Stress_bottom_full = np.array([Ply_stresses_bottom[0],Ply_stresses_bottom[1],0,
                                        Ply_stresses_bottom_shear[0],Ply_stresses_bottom_shear[1],Ply_stresses_bottom[2]])
            Ply_Lstresses_top = localstress(Stress_top_full,layer[0])
            Ply_Lstresses_bottom = localstress(Stress_bottom_full,layer[0])      
            Stress_top = Ply_Lstresses_top[axis]
            Stress_bottom = Ply_Lstresses_bottom[axis]
            Distribution.append([layer[2],Stress_top])
            Distribution.append([layer[3],Stress_bottom])

        if(theory == 'TSDT'):
            Res_Strains = solver(Modulus,layers,theory,global_loads)
            Ply_Strains_top = laminastrains(Res_Strains,layer[2],theory)
            Ply_Strains_bottom = laminastrains(Res_Strains,layer[3],theory)
            Q_bar,Q_shear = Constitutive(Modulus[0],Modulus[1],
            Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
            Ply_stresses_top = np.dot(Q_bar,Ply_Strains_top[:3])
            Ply_stresses_top_shear = np.dot(Q_shear,Ply_Strains_top[3:])
            Ply_stresses_bottom = np.dot(Q_bar,Ply_Strains_bottom[:3])
            Ply_stresses_bottom_shear = np.dot(Q_shear,Ply_Strains_bottom[3:])
            Stress_top_full = np.array([Ply_stresses_top[0],Ply_stresses_top[1],0,
                                        Ply_stresses_top_shear[0],Ply_stresses_top_shear[1],Ply_stresses_top[2]])
            Stress_bottom_full = np.array([Ply_stresses_bottom[0],Ply_stresses_bottom[1],0,
                                        Ply_stresses_bottom_shear[0],Ply_stresses_bottom_shear[1],Ply_stresses_bottom[2]])
            Ply_Lstresses_top = localstress(Stress_top_full,layer[0])
            Ply_Lstresses_bottom = localstress(Stress_bottom_full,layer[0])      
            Stress_top = Ply_Lstresses_top[axis]
            Stress_bottom = Ply_Lstresses_bottom[axis]
            Distribution.append([layer[2],Stress_top])
            Distribution.append([layer[3],Stress_bottom])
    print(Distribution)
    x = [row[1] for row in Distribution]
    y = [row[0] for row in Distribution]
    plt.plot(x,y,label='Strain')  
    plt.ylim(layers[len(layers)-1][3], layers[0][2])
    plt.xlim(np.min(x),np.max(x))
    plt.axhline(0,linestyle='--', color='black')
    plt.axvline(0, color='black')
    plt.fill_betweenx(y,x,0.0)
    plt.grid(True)
    pylab.show()
    return Distribution
 
#####################Strain Distribution######################################
def strain_distribution2(Modulus,layers,theory,global_loads,axis=0):
    """
    Modulus: (float,array),  [E1,E2,v12,v21,G12,G23,G13]
    layers : (float, array), layers data in the form of [angle,thickness,Zk]
    theory : string, 'CLPT', 'FSDT', 'TSDT'
    global_loads: (float array), [Nxx,Nyy,Nxy,Mxx,Myy,Mxy,Pxx,Pyy,Pxy,Qy,Qx,Ry,Rx]
    axis : 0 -> exx, 1-->eyy, 2--> gamma_xy, 3-->gamma_yz, 4-->gamma_xz
    OUTPUTS:
    
    Distribution : (float) array with load vs thickness
    """
    Distribution_s=[]
    N=10
    for i, layer in enumerate(layers):
        if(theory == 'CLPT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Strain = Ply_Strains[axis]
                Distribution_s.append([z,Strain])
        if(theory == 'FSDT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Strain = Ply_Strains[axis]
                Distribution_s.append([z,Strain])
        if(theory == 'TSDT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Strain = Ply_Strains[axis]
                Distribution_s.append([z,Strain])

    print(Distribution_s)
    x = [row[1] for row in Distribution_s]
    y = [row[0] for row in Distribution_s]
    plt.plot(x,y,label='Strain')  
    plt.ylim(layers[len(layers)-1][3], layers[0][2])
    plt.xlim(np.min(x),np.max(x))
    plt.axhline(0,linestyle='--', color='black')
    plt.axvline(0, color='black')
    plt.fill_betweenx(y,x,0.0)
    plt.grid(True)
    pylab.show()
    return Distribution_s
####################Control Function##########################################
def stress_distribution2(Modulus,layers,theory,global_loads,axis=0):
    """
    Modulus: (float,array),  [E1,E2,v12,v21,G12,G23,G13]
    layers : (float, array), layers data in the form of [angle,thickness,Zk]
    theory : string, 'CLPT', 'FSDT', 'TSDT'
    global_loads: (float array), [Nxx,Nyy,Nxy,Mxx,Myy,Mxy,Pxx,Pyy,Pxy,Qy,Qx,Ry,Rx]
    axis : 0 -> exx, 1-->eyy, 2--> gamma_xy, 3-->gamma_yz, 4-->gamma_xz
    OUTPUTS:
    
    Distribution : (float) array with load vs thickness
    """
    N=10
    Distribution=[]
    for i, layer in enumerate(layers):
        if(theory == 'CLPT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Q_bar = Constitutive(Modulus[0],Modulus[1],
                Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
                Ply_stresses = np.dot(Q_bar,Ply_Strains)
                Stress = Ply_stresses[axis]
                Distribution.append([z,Stress])

        if(theory == 'FSDT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)                
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Q_bar,Q_shear = Constitutive(Modulus[0],Modulus[1],
                Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
                Ply_stresses = np.dot(Q_bar,Ply_Strains[:3])
                Ply_stresses_shear = np.dot(Q_shear,Ply_Strains[3:])
                Stress_full = np.concatenate((Ply_stresses,Ply_stresses_shear))
                Stress = Stress_full[axis]
                Distribution.append([z,Stress])

        if(theory == 'TSDT'):
            for j in range(N+1):
                z =layer[2]+j*(abs(layer[3]-layer[2])/N)
                Res_Strains = solver(Modulus,layers,theory,global_loads)
                Ply_Strains = laminastrains(Res_Strains,z,theory)
                Q_bar,Q_shear = Constitutive(Modulus[0],Modulus[1],
                Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
                Ply_stresses = np.dot(Q_bar,Ply_Strains[:3])
                Ply_stresses_shear = np.dot(Q_shear,Ply_Strains[3:])
                Stress_full = np.concatenate((Ply_stresses,Ply_stresses_shear))
                Stress = Stress_full[axis]
                Distribution.append([z,Stress])
    print(Distribution)
    x = [row[1] for row in Distribution]
    y = [row[0] for row in Distribution]
    plt.plot(x,y,label='Strain')  
    plt.ylim(layers[len(layers)-1][3], layers[0][2])
    plt.xlim(np.min(x),np.max(x))
    plt.axhline(0,linestyle='--', color='black')
    plt.axvline(0, color='black')
    plt.fill_betweenx(y,x,0.0)
    plt.grid(True)
    pylab.show()
    return Distribution
##############################################################################
                            #VOID#
###########################################################################
def stress_distribution(Modulus,layers,theory,global_loads,axis=0):
    """
    Modulus: (float,array),  [E1,E2,v12,v21,G12,G23,G13]
    layers : (float, array), layers data in the form of [angle,thickness,Zk]
    theory : string, 'CLPT', 'FSDT', 'TSDT'
    global_loads: (float array), [Nxx,Nyy,Nxy,Mxx,Myy,Mxy,Pxx,Pyy,Pxy,Qy,Qx,Ry,Rx]
    """
    Distribution=[]
    for i, layer in enumerate(layers):
        if(theory == 'CLPT'):
            Res_Strains = solver(Modulus,layers,theory,global_loads)
            Ply_Strains_top = laminastrains(Res_Strains,layer[2],theory)
            Ply_Strains_bottom = laminastrains(Res_Strains,layer[3],theory)
            Q_bar = Constitutive(Modulus[0],Modulus[1],
            Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
            Ply_stresses_top = np.dot(Q_bar,Ply_Strains_top)
            Ply_stresses_bottom = np.dot(Q_bar,Ply_Strains_bottom)
            Stress_top = Ply_stresses_top[axis]
            Stress_bottom = Ply_stresses_bottom[axis]
            Distribution.append([layer[2],Stress_top])
            Distribution.append([layer[3],Stress_bottom])

        if(theory == 'FSDT'):
            Res_Strains = solver(Modulus,layers,theory,global_loads)
            Ply_Strains_top = laminastrains(Res_Strains,layer[2],theory)
            Ply_Strains_bottom = laminastrains(Res_Strains,layer[3],theory)
            Q_bar,Q_shear = Constitutive(Modulus[0],Modulus[1],
            Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
            Ply_stresses_top = np.dot(Q_bar,Ply_Strains_top[:3])
            Ply_stresses_top_shear = np.dot(Q_shear,Ply_Strains_top[3:])
            Ply_stresses_bottom = np.dot(Q_bar,Ply_Strains_bottom[:3])
            Ply_stresses_bottom_shear = np.dot(Q_shear,Ply_Strains_bottom[3:])
            Stress_top_full = np.concatenate((Ply_stresses_top,Ply_stresses_top_shear))
            Stress_bottom_full = np.concatenate((Ply_stresses_bottom,Ply_stresses_bottom_shear))
            Stress_top = Stress_top_full[axis]
            Stress_bottom = Stress_bottom_full[axis]
            Distribution.append([layer[2],Stress_top])
            Distribution.append([layer[3],Stress_bottom])

        if(theory == 'TSDT'):
            Res_Strains = solver(Modulus,layers,theory,global_loads)
            Ply_Strains_top = laminastrains(Res_Strains,layer[2],theory)
            Ply_Strains_bottom = laminastrains(Res_Strains,layer[3],theory)
            Q_bar,Q_shear = Constitutive(Modulus[0],Modulus[1],
            Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],[layer],theory,0)
            Ply_stresses_top = np.dot(Q_bar,Ply_Strains_top[:3])
            Ply_stresses_top_shear = np.dot(Q_shear,Ply_Strains_top[3:])
            Ply_stresses_bottom = np.dot(Q_bar,Ply_Strains_bottom[:3])
            Ply_stresses_bottom_shear = np.dot(Q_shear,Ply_Strains_bottom[3:])
            Stress_top_full = np.concatenate((Ply_stresses_top,Ply_stresses_top_shear))
            Stress_bottom_full = np.concatenate((Ply_stresses_bottom,Ply_stresses_bottom_shear))
            Stress_top = Stress_top_full[axis]
            Stress_bottom = Stress_bottom_full[axis]
            Distribution.append([layer[2],Stress_top])
            Distribution.append([layer[3],Stress_bottom])
    print(Distribution)
    x = [row[1] for row in Distribution]
    y = [row[0] for row in Distribution]
    plt.plot(x,y,label='Strain')  
    plt.ylim(layers[len(layers)-1][3], layers[0][2])
    plt.xlim(np.min(x),np.max(x))
    plt.axhline(0,linestyle='--', color='black')
    plt.axvline(0, color='black')
    plt.fill_betweenx(y,x,0.0)
    plt.grid(True)
    pylab.show()
    return Distribution
##################Ply Local stresses Transformation###########################
def localstress(load,theta):
    """ Returns the Ply strains relative to the principal axis 1 and 2,
        given the Ply strains relative to the x,y axis
        and the fibres angle in the Ply
     INPUTS:
     load  : (float vector), Ply strains relative to X,Y global axis.
     theta : (float), Ply fibres angle.

     OUTPUTS:
     el     : (float vector), Ply strains relative to the Principal axis 1 and 2.
     """
    m =  np.cos(np.radians(theta))
    n = np.sin(np.radians(theta))
    T = np.array([[m**2, n**2, 0, 0, 0, 2*n*m],
                  [n**2, m**2, 0, 0, 0, -2*n*m],
                  [0, 0, 1, 0, 0, 0],
                  [0, 0, 0, m, -n, 0],
                  [0, 0, 0, n, m, 0],
                  [-m*n, m*n, 0, 0, 0,m**2-n**2]])
    el = np.dot(T,load)
    return el
##################Ply Local strains Transformation###########################
def localstrains(load,theta):
    """ Returns the Ply strains relative to the principal axis 1 and 2,
        given the Ply strains relative to the x,y axis
        and the fibres angle in the Ply
     INPUTS:
     load  : (float vector), Ply strains relative to X,Y global axis.
     theta : (float), Ply fibres angle.

     OUTPUTS:
     el     : (float vector), Ply strains relative to the Principal axis 1 and 2.
     """
    m =  np.cos(np.radians(theta))
    n = np.sin(np.radians(theta))
    T = np.array([[m**2, n**2, 0, 0, 0, n*m],
                  [n**2, m**2, 0, 0, 0, -n*m],
                  [0, 0, 1, 0, 0, 0],
                  [0, 0, 0, m, -n, 0],
                  [0, 0, 0, n, m, 0],
                  [-2*m*n, 2*m*n, 0, 0, 0,m**2-n**2]])
    el = np.dot(T,load)
    return el
#################Ply Strains From Laminate Strains#############################
def laminastrains(global_strains,z,theory):
    """ Returns the Ply strains, given the location of the Ply midplane,
    midplane strains, and the global curvature
    INPUTS:
    e     : (float vector), midplane strains.
    z     : (float), midplane location relative to the global axis.
    k     : (float vector), midplane curvatures.
    global_strains: (float) vector with global strains in the following order:
         CLPT:[exx0,eyy0,gamma_xy0,exx1,eyy1,gamma_xy1]
         FSDT:[exx0,eyy0,gamma_xy0,exx1,eyy1,gamma_xy1,gamma_yz0,gamma_xz0]
         TSDT:[exx0,eyy0,gamma_xy0,exx1,eyy1,gamma_xy1
                                    ,exx3,eyy3,gamma_xy3
                                    ,gamma_yz0,gamma_xz0
                                    ,gamma_yz2,gamma_xz2]
    OUTPUTS: (float vector), Ply strains relative to the global axis.
            with the following order:
            {exx, eyy, gamma_xy, gamma_yz,gamma_xz}
    """
    if(theory == 'CLPT'):
        e = global_strains[:3]
        k = global_strains[3:]
        # CLPT returns {exx, eyy,ezz,gamma_yz,gamma_xz, gamma_xy}
        return e +(z * k)

    if(theory == 'FSDT'):
        e = np.array([global_strains[0],global_strains[1],global_strains[2],
            global_strains[6],global_strains[7]])
        k = np.array([global_strains[3],global_strains[4],global_strains[5],0.0,0.0])
        # FSDT returns {exx, eyy,ezz,gamma_yz,gamma_xz, gamma_xy}
        return e +(z * k)

    if(theory == 'TSDT'):
        e = np.array([global_strains[0],global_strains[1],global_strains[2]])
        k1 = np.array([global_strains[3],global_strains[4],global_strains[5]])
        k2 = np.array([global_strains[6],global_strains[7],global_strains[8]])
        e_shear = np.array([global_strains[9],global_strains[10]])
        k_shear = np.array([global_strains[11],global_strains[12]])
        strains = e + (z*k1)+(z**3 * k2)
        strains_shear = e_shear +(z**2 * k_shear)
        # TSDT returns {exx, eyy,ezz,gamma_yz,gamma_xz, gamma_xy}
        return np.concatenate((strains,strains_shear))
#################Solver########################################################
def solver(Modulus,layers,theory,global_loads):
    """ return Global laminate strains, given global laminate resultant stresses
    INPUTS:
    
    Modulus: (float,array),  [E1,E2,v12,v21,G12,G23,G13]
    layers : (float, array), layers data in the form of [angle,thickness,Zk]
    theory : string, 'CLPT', 'FSDT', 'TSDT'
    global_loads: (float array), [Nxx,Nyy,Nxy,Mxx,Myy,Mxy,Pxx,Pyy,Pxy,Qy,Qx,Ry,Rx]
    
    OUTPUT:
    CLPT:
    (Float) concatenated array of [exx0,eyy0,gamma_xy0,exx1,eyy1,gamma_xy1]
    FSDT:
    (Float) concatenated array of [exx0,eyy0,gamma_xy0,exx1,eyy1,gamma_xy1
                                    ,gammayz_0,gammaxz_0] 
    TSDT:
    (Float) concatenated array of [exx0,eyy0,gamma_xy0,exx1,eyy1,gamma_xy1
                                    ,exx3,eyy3,gamma_xy3
                                    ,gamma_yz0,gamma_xz0
                                    ,gamma_yz2,gamma_xz2]
    """
    if(theory == 'CLPT'):
        Const_Matrix = Constitutive(Modulus[0],Modulus[1],
            Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],layers,theory,1)
        Resultant = global_loads
        condition=np.linalg.cond(Const_Matrix)
        if (condition > 10**7):
            print("Constitutive Matrix is ill-conditioned, Regularziation least square method is used, please check the accuracy of the solution")
            Solution = scipy.sparse.linalg.lsmr(Const_Matrix,Resultant,atol=1e-10)[0]
        else:
            Solution = np.linalg.solve(Const_Matrix,Resultant)
        return Solution

    if(theory == 'FSDT'):
        K = 5/6
        Const_Matrix,Const_Matrix_shear = Constitutive(Modulus[0],Modulus[1]
            ,Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],layers,theory,1)
        Resultant = global_loads[:6]
        Resultant_shear = global_loads[6:]
        condition=np.linalg.cond(Const_Matrix)
        if (condition > 10**7):
            print("Constitutive Matrix is ill-conditioned, Regularziation least square method is used, please check the accuracy of the solution")
            Solution = scipy.sparse.linalg.lsmr(Const_Matrix,Resultant,atol=1e-10)[0]
        else:
            Solution = np.linalg.solve(Const_Matrix,Resultant)
        Solution_shear = np.linalg.solve(K*Const_Matrix_shear,Resultant_shear)
        return np.concatenate((Solution,Solution_shear))
    if(theory == 'TSDT'):
        Const_Matrix,Const_Matrix_shear = Constitutive(Modulus[0],Modulus[1]
            ,Modulus[2],Modulus[3],Modulus[4],Modulus[5],Modulus[6],layers,theory,1)
        Resultant = global_loads[:9]
        Resultant_shear = global_loads[9:]
        condition=np.linalg.cond(Const_Matrix)
        if (condition > 10**20):
            print("Constitutive Matrix is ill-conditioned, Regularziation least square method is used, please check the accuracy of the solution")
            Solution = scipy.sparse.linalg.lsmr(Const_Matrix,Resultant,atol=1e-10)[0]
        else:
            Solution = np.linalg.solve(Const_Matrix,Resultant)           
        Solution_shear = np.linalg.solve(Const_Matrix_shear,Resultant_shear)
        return np.concatenate((Solution,Solution_shear))
#################Laminated plate Constitutive matrix#############################
def Constitutive(E1,E2,v12,v21,G12,G23,G13,layers,theory,mode):
    """Returns the ply lamina stiffness matrix 'Q-bar' given the fibre angle
       and orthotropic Modulii of the ply when mode =0.
       and returns the Constitutive block matrix when mode=1
       INPUTS:
       E1    : (float), Elastic Modulus in the fibres loading direction.
       E2    : (float), Elastic Modulus in the direction perpendicular to the
       fibres.
       v12   : (float), Poisson's ratio 1-2 loading directions
       v21   : (float), Poisson's ratio 2-1 loading directions
       G12   : (float), Shear Modulus
       G23   : (float), Shear Modulus
       G13   : (float), Shear Modulus
       theory : string, 'CLPT', 'FSDT', 'TSDT'
       mode  : (integer), 0 for Q stiffness, 1 to return Constitutive matrix
       layers: (float), fibres angles, Plys thickness, Ply midplane relative
       to the reference.
       theory: (String), takes any of these values :'CLPT' or 'FSDT' or 'TSDT'
       OUTPUTS:
       if mode=0
       Q_bar: (float), array with Q stiffness matrix.
       Q_shear:(float), array with Q shear stiffness matrix.
       
       if mode=1
       Const_Matrix: (float) Array, Constitutive relation matrix.
       Shear_Matrix: (float) Array, Shear Consitutive relation matrix.
       """
    #       A     : (float array), Extenstional stiffness matrix.
    #       B     : (float array), Coupling stiffness matrix.
    #       D     : (float array), Bending stiffness matrix.
       
    A11, A12, A22, A16, A26, A66 = 0, 0, 0, 0, 0, 0
    B11, B12, B22, B16, B26, B66 =0, 0, 0, 0, 0, 0
    D11, D12, D22, D16, D26, D66 =0, 0, 0, 0, 0, 0
    if(theory == 'FSDT' or theory == 'TSDT' ):
        A44, A45, A55  =  0, 0, 0
    if(theory == 'TSDT' ):
        E11, E12, E22, E16, E26, E66 =0, 0, 0, 0, 0, 0
        F11, F12, F22, F16, F26, F66 =0, 0, 0, 0, 0, 0
        H11, H12, H22, H16, H26, H66 =0, 0, 0, 0, 0, 0
        D44, D45, D55 = 0, 0, 0
        F44, F45, F55 = 0, 0, 0
    # Compilance Matrix######################################################
    # Reduces stiffness matrix
    Q11 = E1/(1-v12*v21)
    Q22 = E2/(1-v12*v21)
    Q12 = v12*E2/(1-v12*v21)
    Q66 = G12
    if(theory == 'FSDT' or theory == 'TSDT' ):
        # First Order Shear Deformation Theory.
        Q44 =G23
        Q55 =G13
    for n, comp in enumerate(layers):
        # Fibre Angles###########################################################
        m =  np.cos(np.radians(comp[0]))
        n = np.sin(np.radians(comp[0]))
        ##########################################################################
        # Plane stress transformed reduced stiffness matrix
        Q11_ = Q11 * m**4 + 2 * (Q12+2*Q66) * m**2 * n**2 + Q22 * n**4
        Q12_ = (Q11 + Q22 - 4 * Q66) * m**2 * n**2 + Q12 * (m**4 + n**4)
        Q22_ = Q11 * n**4 + 2 * (Q12 + 2 * Q66) * m**2 * n**2 + Q22 * m**4
        Q16_ = (Q11 - Q12 - 2* Q66) * m**3 * n +(Q12 - Q22 + 2 * Q66)* m * n**3
        Q26_ = (Q11 - Q12 - 2 * Q66) * n**3 * m + (Q12 - Q22 + 2 * Q66) * n * m**3
        Q66_ = (Q11 + Q22 - 2 * Q12 - 2 * Q66) * m**2 * n**2 + Q66 *(m**4 + n**4)
        if(theory == 'FSDT' or theory == 'TSDT' ):
            #First& Third Order Shear Deformation Theory.
            Q44_ = Q44 * m**2 + Q55 * n**2
            Q45_ = (Q55-Q44) * m * n
            Q55_ = Q55 * m**2 + Q44 * n**2
        if(theory == 'CLPT' and mode == 0):
            Q_bar = np.array([[Q11_,Q12_,Q16_],[Q12_, Q22_, Q26_],[Q16_, Q26_,Q66_]])
            return Q_bar
        if(theory == 'FSDT' and mode == 0):
            Q_bar = np.array([[Q11_,Q12_,Q16_],[Q12_, Q22_, Q26_],[Q16_, Q26_,Q66_]])
            Q_shear = np.array([[Q44_,Q45_],[Q45_, Q55_]])
            return Q_bar,Q_shear

        if(theory == 'TSDT' and mode == 0):
            Q_bar = np.array([[Q11_,Q12_,Q16_],[Q12_, Q22_, Q26_],[Q16_, Q26_,Q66_]])
            Q_shear = np.array([[Q44_,Q45_],[Q45_, Q55_]])
            return Q_bar,Q_shear

        if(mode == 1):
           # Constitutive Relation####################################################
            # Extensional Stiffness terms
            #comp[0]--> fiber angle, comp[1]--> thickness of plate, comp[2]--> Zk , comp[3]-->zk+1
            A11 += Q11_ * comp[1]
            A12 += Q12_ * comp[1]
            A22 += Q22_ * comp[1]
            A16 += Q16_ * comp[1]
            A26 += Q26_ * comp[1]
            A66 += Q66_ * comp[1]
            # Coupling Stiffness terms
            B11 += (1/2) * Q11_ * (comp[3]**2 - comp[2]**2)
            B12 += (1/2) * Q12_ * (comp[3]**2 - comp[2]**2)
            B22 += (1/2) * Q22_ * (comp[3]**2 - comp[2]**2)
            B16 += (1/2) * Q16_ * (comp[3]**2 - comp[2]**2)
            B26 += (1/2) * Q26_ * (comp[3]**2 - comp[2]**2)
            B66 += (1/2) * Q66_ * (comp[3]**2 - comp[2]**2)

            D11 += (1/3) *  Q11_ * (comp[3]**3 - comp[2]**3)
            D12 += (1/3) *  Q12_ * (comp[3]**3 - comp[2]**3)
            D22 += (1/3) *  Q22_ * (comp[3]**3 - comp[2]**3)
            D16 += (1/3) *  Q16_ * (comp[3]**3 - comp[2]**3)
            D26 += (1/3) *  Q26_ * (comp[3]**3 - comp[2]**3)
            D66 += (1/3) *  Q66_ * (comp[3]**3 - comp[2]**3)
            if(theory == 'TSDT' ):
                E11 += (1/4) *  Q11_ * (comp[3]**4 - comp[2]**4)
                E12 += (1/4) *  Q12_ * (comp[3]**4 - comp[2]**4)
                E22 += (1/4) *  Q22_ * (comp[3]**4 - comp[2]**4)
                E16 += (1/4) *  Q16_ * (comp[3]**4 - comp[2]**4)
                E26 += (1/4) *  Q26_ * (comp[3]**4 - comp[2]**4)
                E66 += (1/4) *  Q66_ * (comp[3]**4 - comp[2]**4)

                F11 += (1/5) *  Q11_ * (comp[3]**5 - comp[2]**5)
                F12 += (1/5) *  Q12_ * (comp[3]**5 - comp[2]**5)
                F22 += (1/5) *  Q22_ * (comp[3]**5 - comp[2]**5)
                F16 += (1/5) *  Q16_ * (comp[3]**5 - comp[2]**5)
                F26 += (1/5) *  Q26_ * (comp[3]**5 - comp[2]**5)
                F66 += (1/5) *  Q66_ * (comp[3]**5 - comp[2]**5)

                H11 += (1/7) *  Q11_ * (comp[3]**7 - comp[2]**7)
                H12 += (1/7) *  Q12_ * (comp[3]**7 - comp[2]**7)
                H22 += (1/7) *  Q22_ * (comp[3]**7 - comp[2]**7)
                H16 += (1/7) *  Q16_ * (comp[3]**7 - comp[2]**7)
                H26 += (1/7) *  Q26_ * (comp[3]**7 - comp[2]**7)
                H66 += (1/7) *  Q66_ * (comp[3]**7 - comp[2]**7)
            if(theory == 'FSDT' or theory == 'TSDT' ):
                A44 += Q44_ * comp[1]
                A45 += Q45_ * comp[1]
                A55 += Q55_ * comp[1]
            if(theory == 'TSDT' ) :
                D44 += (1/3) * Q44_ * (comp[3]**3 - comp[2]**3)
                D45 += (1/3) * Q45_ * (comp[3]**3 - comp[2]**3)
                D55 += (1/3) * Q55_ * (comp[3]**3 - comp[2]**3)
                
                F44 += (1/5) * Q44_ * (comp[3]**5 - comp[2]**5)
                F45 += (1/5) * Q45_ * (comp[3]**5 - comp[2]**5)
                F55 += (1/5) * Q55_ * (comp[3]**5 - comp[2]**5)

    # The Extensional stiffness matrix 'A' relates the resultant in-plane
        #forces to the in-plane strains
    A = np.array([[A11, A12, A16],[A12, A22, A26],[A16, A26, A66]])
    # The Extensional stiffness matrix 'A,D,F' for Third order shear relates
    #the resultant in-plane forces to the in-plane strains
    if(theory == 'FSDT' or theory == 'TSDT' ):
        A_shear = np.array([[A44, A45],[A45, A55]])
    if(theory == 'TSDT' ):
        D_shear = np.array([[D44, D45],[D45, D55]])
        F_shear = np.array([[F44, F45],[F45,F55]])
    # The Coupling stiffness matrix 'B' couples the forces and moment terms to
    #the midplane strains and midplane curvatures
    B = np.array([[B11, B12, B16],[B12, B22, B26],[B16, B26, B66]])
    # The Bending stiffness matrix 'D' relates the resultant bending moments
    #to the plate curvatures
    D = np.array([[D11, D12, D16],[D12, D22, D26],[D16, D26, D66]])
    if(theory == 'TSDT' ):
        E = np.array([[E11, E12, E16],[E12, E22, E26],[E16, E26, E66]])
        F = np.array([[F11, F12, F16],[F12, F22, F26],[F16, F26, F66]])
        H = np.array([[H11, H12, H16],[H12, H22, H26],[H16, H26, H66]])

    if(theory == 'CLPT'):
        Const_Matrix = np.bmat([[A,B],[B,D]])
        return Const_Matrix

    if(theory == 'FSDT'):
        Const_Matrix = np.bmat([[A,B],[B,D]])
        Shear_Matrix = A_shear
        return Const_Matrix, Shear_Matrix

    if(theory == 'TSDT'):
        Const_Matrix = np.bmat([[A,B,E],[B,D,F],[E,F,H]])
        Shear_Matrix = np.bmat([[A_shear,D_shear],[D_shear,F_shear]])
        return Const_Matrix, Shear_Matrix

##########################Generate Layers Vector###############################
def LayersT(stack1,repeat1,stack2,repeat2,thickness):
    """
    INPUTS  :
    
    stack1  : (Float) Array, with the laying angles
    repeate1: (Integer), Number of repetations to stack1
    stack2  : (Float) Array, with the laying angles
    repeate2: (Integer), Number of repetations to stack1
    
    OUTPUTS :
    
    (Float) Array with [angle, thickness of ply, top Ply coordinate, bottom Ply
                        coordinate]
    """
    layers=[]
    Nolaminas = (len(stack1) * repeat1)+(len(stack2)*repeat2)
    topH = -(Nolaminas/2)*thickness
    Z = topH
    for i in range(repeat1):
        for j in range(len(stack1)):
            layers.append([stack1[j],thickness,Z,Z+thickness])
            Z += thickness
    for k in range(repeat2):
        for m in range(len(stack2)):
            layers.append([stack2[m],thickness,Z,Z+thickness])
            Z += thickness
    return np.asarray(layers)

Modulus,layers = Paperdata()
#Modulus,layers = TestReddy()
#Modulus,layers = TestCLPT()
#stress_distribution(Modulus,layers,'CLPT',np.array([1000,0,0,1000,0,0]),3)
#strain_distribution(Modulus,layers,'CLPT',np.array([0,0,0,-2.5,0,0]),0)
CLPT=local_stress_distribution2(Modulus,layers,'CLPT',np.array([1684094,0,0,116536,0,0]),1)
FSDT=local_stress_distribution2(Modulus,layers,'FSDT',np.array([1684094,0,0,116536,0,0,0,0]),1)
TSDT=local_stress_distribution2(Modulus,layers,'TSDT',np.array([1684094,0,0,116536,0,0,0,0,0,0,0,0,1]),0)
#strain_distribution(Modulus,layers,'FSDT',np.array([250,0,0,0,0,0,0,0]),0)
#strain_distribution(Modulus,layers,'TSDT',np.array([250,0,0,0,0,0,0,0,0,0,0,0,0]),0)
#strain_distribution(Modulus,layers,'FSDT',np.array([1684094,0,0,116536,0,0,0,0]),1)
#strain_distribution_distribution(Modulus,layers,'TSDT',np.array([1684094,0,0,116536,0,0,0,0,0,0,0,0,0]),1)
theory =np.array([CLPT,FSDT,TSDT])
legend =np.array(["CLPT","FSDT","TSDT"])
for i in range(3):
    x = [row[1] for row in theory[i]]
    y = [row[0] for row in theory[i]]
    plt.plot(x,y,label=legend[i])  
plt.ylim(layers[len(layers)-1][3], layers[0][2])
plt.xlim(np.min(x),np.max(x))
plt.axhline(0,linestyle='--', color='black')
plt.axvline(0, color='black')
#    plt.fill_betweenx(y,x,0.0)
plt.grid(True)
plt.ylabel('z-Thickness co-ordinates')
plt.xlabel('$\sigma_1$')
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
pylab.show()