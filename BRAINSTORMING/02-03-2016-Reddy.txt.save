BOOK  :MECHANICS OF LAMINATED COMPOSITE PLATES AND SHELLS
AUTHOR: J.N.REDDY
---------------------------------------------------------
BOOK REVIEW
===========

CHAPTER 1
=========

MATERIALS CAN BE STUDIED AT VARIOUS SCALES:
	1) ATOMIC LEVEL.
	2) NANO-LEVEL.
	3) SINGLE-CRYSTAL LEVEL.
	4) GROUP OF CRYSTALS.
	5) GRAINS.
	6) FIBRES/MATRIX.
	7) REPRESENTATIVE VOLUME.

THIS BOOK CONSIDERS BASIC UNIT OF MATERIAL THAT HAS PROPERTIES SUCH AS:
	1) MODULUS OF ELASTICITY.
	2) STRENGTH.
	3) THERMAL COEFFICIENT OF EXPANSION.
	4) ELECTRICAL RESISTANCE.
	5) ETC..

OBJECTIVE OF THE BOOK:
---------------------

ANALYSIS OF LAMINATED COMPOSITE STRUCTURES USING:
	1) AN-ISOTROPIC ELASTICITY EQUATIONS.
	2) STRUCTURAL THEORIES.
	3) ANALYTICAL AND COMPUTATIONAL METHODS.
	4) DAMAGE/FAILURE THEORIES.


ADDITIONAL KNOWLEDGE NEEDED:
----------------------------
- VECTORS
- TENSORS
- INTEGRAL RELATIONS
- EQUATIONS GOVERNING DEFORMABLE AND ANISOTROPIC MEDIUM
- VIRTUAL WORK PRINCIPLE
- VARIATIONAL METHODS

-----------------------------------------------------------------------------

1)VECTORS AND TENSORS
=====================

- SPECIFIYING NATURAL LAWS IN VECTOR NOTATION LEAVES THEM *INVARIANT* TO COORDINATE TRANSFORMATIONS.

---------------------------------------------------------------------------------------------------------------------------------------------+
WHAT IS INVARIANCE?															     |
---------------------------------------------------------------------------------------------------------------------------------------------|
SIGMA = R SIGMA R^SIGMA															     |
																	     |
SPECTRAL DECOMPOSITION ( SOLVE EIGEN VALUE PROBLEM)											     |
DET(SIGMA - LAMBDA I) = 0														     |
																	     |
THE ROOTS ARE THE EIGEN VALUES														     |
																	     |
-lambda^3 + I1 lambda^2 + I2 lambda +I3 = 0												     |
																	     |
I1, I2 and I3 are the invariants of the stress tensor. they are called invariants because they don't change with the co-ordinate system.     |
the stresses will change to the new directions but the invariance won't change.								     |
																	     |
THIS IS USEFUL FOR THE CONSTITUTIVE MODELS, IF WE ROTATE THE BODY WE DON'T EXPECT IT TO CHANGE OTHER WISE WE NEED A NEW MODEL.		     |
---------------------------------------------------------------------------------------------------------------------------------------------+

VECTORS:
=======

1) GENERAL FORM OF VECTORS:
--------------------------

LET A BE OUR VECTOR
(a1,a2,a3) --> vector components
(e1,e2,e3) --> basis vectors ( not unit vectors)

	A = a1e1 + a2e2 + a3e3

CARTESIAN COORDINATE SYSTEM:
---------------------------
The system is called cartesian IF BASIS VECTORS e1, e2 and e3  has :
	1) Fixed length.
	2) Fixed direction.

	RECTANGULAR CARTESIAN
	---------------------
	The system is rectangular cartesian if basis vector e1, e2 and e3 are orthogonal it's called 'RECTANGULAR CARTESIAN'.
	
	ORTHONORMAL CARTESIAN
	---------------------
	The system is orthonormal cartesian if basis vector e1, e2 and e3 are:
		1) Unit lengths.
		2) Mutually orthogonal.


NOTE:
----
- SCALAR PRODUCT --> DOT PRODUCT
- VECTOR PRODUCT --> CROSS PRODUCT

BOTH DOT AND CROSS PRDOCUTS CAN BE EXPRESSED IN THE INDEX FORM USING " KRONECKER DELTA SYMBOL /DELTA IJ AND THE "ALTERNATING SYMBOL" (OR PERMUTATION SYMBOL) /EIPS IJK


- TOTAL VECTOR AREA OF A CLOSED SURFACE IS ZERO.

